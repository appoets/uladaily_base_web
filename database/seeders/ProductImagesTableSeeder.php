<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Product;
use DB;
class ProductImagesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_images')->truncate();
        DB::table('product_images')->insert([
            [
                'product_id' => 1,
                'url' => asset('/seeddata/1/chopped_walnut.jpeg'),
                'position' => 1
            ],
            [
                'product_id' => 2,
                'url' => asset('/seeddata/1/coconut_flakes.jpeg'),
                'position' => 0
            ],
            [
                'product_id' => 3,
                'url' => asset('/seeddata/1/Bonito_Flakes.jpeg'),
                'position' => 0
            ],
            [
                'product_id' => 4,
                'url' => asset('/seeddata/1/penne_pasta.jpeg'),
                'position' => 0
            ],
            [
                'product_id' => 5,
                'url' => asset('/seeddata/1/cheese_macroni.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 6,
                'url' => asset('/seeddata/1/peas.jpeg'),
                'position' => 1
            ],
            [
                'product_id' => 7,
                'url' => asset('/seeddata/1/salad.jpeg'),
                'position' => 0
            ],
            [
                'product_id' => 8,
                'url' => asset('/seeddata/1/Strawberries.png'),
                'position' => 0
            ],
            [
                'product_id' => 9,
                'url' => asset('/seeddata/1/orange.jpeg'),
                'position' => 0
            ],
            [
                'product_id' => 10,
                'url' => asset('/seeddata/1/blueberries.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 11,
                'url' => asset('/seeddata/2/vanilla.jpeg'),
                'position' => 0
            ],
            [
                'product_id' => 12,
                'url' => asset('/seeddata/2/Blueberry-Waffles-3-1-of-1.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 13,
                'url' => asset('/seeddata/2/bacon.png'),
                'position' => 0
            ],
            [
                'product_id' => 14,
                'url' => asset('/seeddata/2/lofthouse+sugar+cookies12.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 15,
                'url' => asset('/seeddata/2/mini_cookies.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 16,
                'url' => asset('/seeddata/2/exps170843_SD153321D02_05_2b_WEB-696x696.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 17,
                'url' => asset('/seeddata/2/Strawberry_cake.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 18,
                'url' => asset('/seeddata/2/61ME3vAupgL._SY879_.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 19,
                'url' => asset('/seeddata/2/clean_linen_solid_air_freshener.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 20,
                'url' => asset('/seeddata/2/clean_linen_solid_air_freshener.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 21,
                'url' => asset('/seeddata/3/60239.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 22,
                'url' => asset('/seeddata/3/Build_Bonanza.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 23,
                'url' => asset('/seeddata/3/822d67cb4e7f4dd2b3b514d4f560c77blg.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 24,
                'url' => asset('/seeddata/3/443394.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 25,
                'url' => asset('/seeddata/3/GUEST_04972e3a-fdf9-4032-a225-d272899f37d3.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 26,
                'url' => asset('/seeddata/3/Classic_Roast_Medium_Ground_Coffee.jpeg'),
                'position' => 0
            ],
            [
                'product_id' => 27,
                'url' => asset('/seeddata/3/2b4ebbf3-7ade-47cb-ba9f-dc80115170ed_1.67fb67e5d921e3bf73d3d65d5a182aca.jpeg'),
                'position' => 1
            ],
            [
                'product_id' => 28,
                'url' => asset('/seeddata/3/care.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 29,
                'url' => asset('/seeddata/3/garnier.jpeg'),
                'position' => 0
            ],
            [
                'product_id' => 30,
                'url' => asset('/seeddata/3/71ONQUlvB3L._SL1300_.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 31,
                'url' => asset('/seeddata/4/tuna.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 32,
                'url' => asset('/seeddata/4/51Yb9yMSGtL.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 33,
                'url' => asset('/seeddata/4/HVR_Ranch_light-lbox-373x400-FFFFFF.png'),
                'position' => 1
            ],
            [
                'product_id' => 34,
                'url' => asset('/seeddata/4/sause.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 35,
                'url' => asset('/seeddata/4/american_singles.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 36,
                'url' => asset('/seeddata/4/3384081937_7c851c91df_o.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 37,
                'url' => asset('/seeddata/4/meat.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 37,
                'url' => asset('/seeddata/4/boneless.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 38,
                'url' => asset('/seeddata/4/86936_640_86936clean_640.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 39,
                'url' => asset('/seeddata/4/sause.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 41,
                'url' => asset('/seeddata/5/IMG_4547Debi-Lily-flowersDebi-Lily-flowers.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 42,
                'url' => asset('/seeddata/5/Roses_Red.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 43,
                'url' => asset('/seeddata/5/Fresh-Pick-of-the-Day-Mixed-Tulips_Bespoke__70785.1518401450.876.999.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 44,
                'url' => asset('/seeddata/5/71OX22BXdPL._SY355_.jpg'),
                'position' => 1
            ],
            [
                'product_id' => 45,
                'url' => asset('/seeddata/5/81voOnGulFL._SX425_.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 46,
                'url' => asset('/seeddata/5/516vC5h776L._AC_SY400_.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 47,
                'url' => asset('/seeddata/5/download.jpg'),
                'position' => 0
            ],
            [
                'product_id' => 48,
                'url' => asset('/seeddata/5/multi-grain-cheerios.png'),
                'position' => 0
            ],
            [
                'product_id' => 49,
                'url' => asset('/seeddata/5/450.jpg'),
                'position' => 1
            ],
            
            
        ]);        
    }
}
