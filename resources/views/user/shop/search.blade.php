@extends('user.layouts.app')
@section('content')
<section class="profile-baner">
	<div class="container">
		<div class="row align-items">
			<div class="col-md-4">
				@forelse($shop as $key=>$Shop)
				<div class="store-item text-center">
					<img src="{{$Shop->default_banner}}" class="img-fluid">
					<div class="p-inner">
						<h5>{{$Shop->name}}</h5>
						<div class="cat">{{$Shop->description}}</div>
					</div>
				</div>
				@empty
				@endforelse
			</div>
			<div class="col-md-8">
                <form action="/product/search" method="get" class="expanding-search-form">
					<div class="search-dropdown">
						<a class="button dropdown-toggle" type="button">
							<span class="toggle-active">Category</span>
							<span class="ion-arrow-down-b"></span>
						</a>
						<!-- <ul class="dropdown-menu">
							<li class="menu-active"><a href="#">Category</a></li>
							<li><a href="#">Produce</a></li>
							<li><a href="#">Alchocol</a></li>
							<li><a href="#">Fruits</a></li>
						</ul> -->
						 <select class="dropdown-menu" name="cat" required>
                                <option>Category</option>
                             @foreach($all_category as $cate)
                                 <option value="{{$cate->id}}">{{$cate->name}}</option>
                             @endforeach
                        </select>
					<!-- </div>
					<input class="search-input" id="global-search" type="search" placeholder="Search">
					<label class="search-label" for="global-search">
						<span class="sr-only">Global Search</span>
					</label>
					<button class="button search-button" type="button">
						<span class="fa fa-search">
						</span>
					</button>
				</form> -->
				 	</div>
                    <input class="search-input" name="shop" value="{{$Shop->id}}" type="hidden">
                    <input class="search-input" name="prodname"  id="global-search" type="text"  placeholder="Search">
                      <button  type="submit"><span class="fa fa-search"></span></button>
                </form>
			</div>
		</div>
	</div>
</section>
<div class="container baner-tab">
	<div class="row has-botom-border align-items">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#tabs-home" role="tab">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#tabs-dept" role="tab">Department</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#tabs-coupon" role="tab">Coupons</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#tabs-items" role="tab">Your Items</a>
			</li>
		</ul>
		<p class="ml-auto">Delivery to <span class="text-green">9001</span> <span class="pl-3">Today, 11AM -
				11PM</span><span class="info-icon">&#x1F6C8;</span></p>
	</div>
</div>

<section>
	<div class="tab-content">
		<div class="tab-pane active" id="tabs-home" role="tabpanel">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="search-left">
							<div class="filter-box">
								<div class="filter-header">
									<p><b>Filter By</b></p>
									<p class="ml-auto">Reset</p>
								</div>
								<div class="filter-block has-botom-border">
									<h5>Departments</h5>
								@forelse($subCategory as $Category)
									<div class="filter-iner-block">
										<h6>{{$Category->name}}</h6>
										<form class="filter-checks">
											@forelse($Category->subcategories as $Subcategory)
											
												<label class="filter-checkbox">{{ $Subcategory->name }}
													<input type="checkbox"  class="filter_all sub_cat"  value="{{ $Subcategory->parent_id }}" >
													<span class="checkmark"></span>
												</label>
												@empty
											@endforelse
									  </form>
									</div>
									<input type="hidden" id="get_shop_id" value="{{$Category->shop_id}}">

								@empty
								@endforelse
									<input type="hidden" id="get_cat_id" value="{{$current_category}}">

								</div>	


									<!-- <div class="filter-iner-block">
										<h6>Dairy & Snacks</h6>
										<form class="filter-checks">
											<label class="filter-checkbox">Milk
												<input type="checkbox" id="pro_filter" name="product_filter[]" value="009" checked="checked">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Soy & Loctos -free
												<input type="checkbox" id="pro_filter" name="product_filter[]">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Packaged Cheese
												<input type="checkbox" id="pro_filter" name="product_filter[]">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Yogurt
												<input type="checkbox" id="pro_filter" name="product_filter[]">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Other Cream
												<input type="checkbox" id="pro_filter" name="product_filter[]" >
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Butter
												<input type="checkbox" id="pro_filter" name="product_filter[]">
												<span class="checkmark"></span>
											</label>
										</form>
									</div> -->
									<!-- <div class="filter-iner-block">
										<h6>Pantry</h6>
										<form class="filter-checks">
											<label class="filter-checkbox">Baking Ingredients
												<input type="checkbox" checked="checked">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Baking Supplies & Decor
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
										</form>
									</div>
									<div class="filter-iner-block">
										<h6>Snacks</h6>
										<form class="filter-checks">
											<label class="filter-checkbox">Candy & Choclate
												<input type="checkbox" checked="checked">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Cookies & Cakes
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Chips & Cakes
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Nuts & Dry Fruits
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
										</form>
									</div> -->

								<div class="filter-block has-botom-border">
									<h5>Sales & Promotions</h5>
									<div class="filter-iner-block">
										<form class="filter-checks">
											<label class="filter-checkbox">Coupons
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
										</form>
									</div>
								</div>
								<div class="filter-block has-botom-border">
									<h5>Brands</h5>
									<div class="filter-iner-block">
										<form class="filter-checks">
										@forelse($filter_brand as $key=>$product)
										

											<label class="filter-checkbox">{{$product->brand}}
												<input type="checkbox" class="filter_all brand" value="{{$product->brand}}">
												<span class="checkmark"></span>
											</label>
											
										@empty
										@endforelse
											
										</form>
									</div>
								</div>
								<div class="filter-block has-botom-border">
									<h5>Nutrition</h5>
									<div class="filter-iner-block">
										<form class="filter-checks">
										@forelse($filter_nutrition as $key=>$product)
											<label class="filter-checkbox">{{$product->nutrition}}
												<input type="checkbox"  class="filter_all nutrition" value="{{$product->nutrition}}">
												<span class="checkmark"></span>
											</label>

										@empty
										@endforelse
										</form>
									</div>
								</div>

								<!-- <div class="filter-block has-botom-border">
									<h5>Brands</h5>
									<div class="filter-iner-block">
										<form class="filter-checks">
											<label class="filter-checkbox">Store Brand
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Slucerny Dairy Forms
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Silk
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Organics
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Blue Diamond
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
										</form>
									</div>
								</div>
								<div class="filter-block has-botom-border">
									<h5>Nutrition</h5>
									<div class="filter-iner-block">
										<form class="filter-checks">
											<label class="filter-checkbox">Organic
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Gluteen Free
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Fat-Free
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Vegan
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Kosher
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
											<label class="filter-checkbox">Sugar - free
												<input type="checkbox">
												<span class="checkmark"></span>
											</label>
										</form>
									</div>
								</div> -->
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="search-right">
							<div class="row m-3 pt-3 has-botom-border">
								<div class="col-6">
									<p>{{$product_count}} results of <b>"{{$product_search_name}}"</b></p>
								</div>
								<div class="col-6">
									<form class="select-box float-right">
										<select>
											<option>Sort by Best Match</option>
										</select>
									</form>
								</div>
							</div>
							<div class="search-content">

								<!-- <div class="row m-3"  id="show_filter"> -->
								<div class="row m-3"  id="show_filter">
								@forelse($product_search as $key=>$search_items)

								@forelse($search_items->products as $key=>$search_item)

									<div class="col-sm-3">

										<div class="item">
											@forelse($search_item->featured_images as $key=>$pro_image)

											<div class="overlay">
												<!-- <a class="overlay-btn" href="#" data-toggle="modal"
													data-target="#myModal">Quick View</a> -->
													 <a class="overlay-btn" href="#" class="product_details"
                                                            data-toggle="modal" data-name="{{$search_item->name}}"
                                                            data-price="{{currencydecimal($search_item->prices->orignal_price)}}/each"
                                                            data-description="{{$search_item->description}}"
                                                            data-img="{{$pro_image->url}}" ,
                                                            data-shop_id="{{$search_item->shop_id}}",
                                                            data-product_id="{{$search_item->id}}",
                                                            data-pro_cart_price="{{($search_item->prices->orignal_price)}}",

                                                            data-directions="{{$search_item->directions}}",
                                                            data-warnings="{{$search_item->warnings}}",
                                                            data-details="{{$search_item->details}}",
                                                            data-ingredients="{{$search_item->ingredients}}",
                                                            data-view_more="{{$search_item->id}}",
                                                            data-related_product="{{$search_item->id}}",
                                                            data-category="{{$search_items->id}}",
                                                            data-target="#myModal">View</a>

												<form action="{{Auth::guest()?url('mycart'):url('addcart')}}"
													method="POST">
													{{csrf_field()}}




													<!-- <label>Select Quantity</label> -->
													<input type="hidden"  value="{{$search_items->id}}" name="shop_id">
													<input type="hidden"  value="{{$search_item->id}}"
														name="product_id">
													<input type="hidden" value="1" name="quantity" class="form-control"
														placeholder="Enter Quantity" readonly min="1" max="100">
													<input type="hidden"  value="{{$search_item->name}}"
														name="name">
													<input type="hidden"  value="{{$search_item->prices->orignal_price}}" name="price" />


													<!-- <button  class="add-btn">@lang('user.add_to_cart')</button> -->
													<button class="overlay-btn-dark" type="submit">Add to Cart </button>

												</form>
												<!-- 					          	<a class="overlay-btn-dark" href="#">Add to Cart</a> -->
											</div>
											<img src="{{$pro_image->url}}" class="img-fluid">
											@empty
											@endforelse
											<div class="p-inner">
												<h5>{{currencydecimal($search_item->prices->orignal_price)}}</h5>
												<h6>{{$search_item->name}}</h6>
												<div class="cat">{{$search_item->description}}</div>
											</div>
										</div>
										
									</div>
									@empty
									@endforelse
								@empty
								@endforelse
								</div>
								
							</div>
						</div>

					</div>


				</div>
			</div>

			<div class="search-bottom">
				<div class="col-md-12 text-center d-flex justify-content-center align-items-baseline py-3">
					<p>Available at other Stores
						<form class="store-dropdown">
							<select>
								<option>Choose Another Store</option>
							</select>
						</form>
					</p>
				</div>
			</div>
			<div class="col-md-12 text-center py-5">
				<p>Search Feedback <span class="text-light">How was your search Experience? </span> <a href="#"
						class="like"><i class="fa fa-thumbs-up"></i></a> <a href="#" class="unlike"><i
							class="fa fa-thumbs-down"></i></a></p>
			</div>
		</div>
	</div>
	</div>
	</div>
	<div class="tab-pane active" id="tabs-dept" role="tabpanel">
		<!-- <p>Dummy text</p> -->
	</div>
	</div>

</section>


<!-- Modal Popup -->
<div class="modal" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body  pt-5">
                <div id="default" class="padding-top0">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="simple-gallery">

                                <img class="maxi" src="#" id="product_image">

								<div class="mini" id="product_image_dimension">
                                <!-- <div class="mini">
                                    <img src="assets/images/Naren.png">
                                    <img src="assets/images/Abi.png">
                                    <img src="assets/images/Preethi.png">
                                    <img src="assets/images/decide.png"> -->
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="product-discription">
                                <h5 id="product_name"></h5>
                                <h6 class="text-green product_price"></h6>
                                <p id="product_decription"></p>
                            </div>
                            <div class="d-flex">
                            <form  action="{{Auth::guest()?url('mycart'):url('addcart')}}" method="POST">
                                    {{csrf_field()}}
                                        <div class="value-button" id="decrease" onclick="decreaseValue()"
                                            value="Decrease Value">-</div>
                                        <input type="number" name="quantity" id="number" name= value="1" />
                                        <div class="value-button" id="increase" onclick="increaseValue()"
                                            value="Increase Value">+</div>


                               
                                    <!-- <label>Select Quantity</label> -->
                                    <input type="hidden" id="shop_id"value="" name="shop_id">
                                    <input type="hidden" id="pro_id" value="" name="product_id">
                                    <!-- <input type="hidden" value="1" name="quantity" class="form-control" placeholder="Enter Quantity" readonly min="1" max="100"> -->
                                    <input type="hidden" id="pro_name" value="" name="name">
                                    <input type="hidden" id="pro_price" value="" name="price" />

                                   
                                     <!-- <button  class="add-btn">@lang('user.add_to_cart')</button> -->
                                     <button class="cart-btn" type="submit" >Add to Cart <i class="fa fa-shopping-cart"></i></button>

                                 
                                    <!-- <a href="#" class="login-item add-btn"
                                        onclick="$('#login-sidebar').asidebar('open')">@lang('user.add_to_cart')</a> -->

                                    <!-- <a href="#" class="login-item add-btn" data-toggle="modal" data-target="#signin1">@lang('user.add_to_cart')</a> -->

                                </form>

                                <!-- <a class="fav-btn" href="#"><i class="fa fa-heart"></i></a> -->
                                <div class="fav-icon">
                                    <div class="icon-wishlist"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tabs-1"
                                            role="tab">Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Ingrediants</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Directions</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">Warnings</a>
                                    </li>
                                </ul><!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                        <p id="product_detail"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                                        <p id="product_ingredient"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                                        <p id="product_direction"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-4" role="tabpanel">
                                        <p id="product_warning"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class=" has-botom-border d-flex">
                                <h6><b>Releated Products</b></h6>
                                <p class="ml-auto">
                                    <a href="/restaurant/details?name={{$Shop->name}}" class="view-link">View More<i class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i></a>
                                </p>
                            </div>
                        </div>

                        <div class="row search-content p-3" id="related_pro">
                            
                           <!--  <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Popup -->

	@endsection


	 @section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModal').on('show.bs.modal', function (e) {

                var product_name = $(e.relatedTarget).attr('data-name');
                var product_price = $(e.relatedTarget).attr('data-price');
                var product_decription = $(e.relatedTarget).attr('data-description');
                var img = $(e.relatedTarget).data('img');
                var shop_id = $(e.relatedTarget).attr('data-shop_id');
                var product_id = $(e.relatedTarget).attr('data-product_id');
                var product_cart_price = $(e.relatedTarget).attr('data-pro_cart_price');

                var product_details = $(e.relatedTarget).attr('data-details');
                var product_directions = $(e.relatedTarget).attr('data-directions');
                var product_warnings= $(e.relatedTarget).attr('data-warnings');
                var product_ingredients = $(e.relatedTarget).attr('data-ingredients');

                $(this).find('#product_name').text(product_name);
                $(this).find('.product_price').text(product_price);
                $(this).find('#product_decription').text(product_decription);
                $("#product_image").attr("src", img);

                $(this).find('#product_detail').text(product_details);
                $(this).find('#product_direction').text(product_directions);
                $(this).find('#product_warning').text(product_warnings);
                $(this).find('#product_ingredient').text(product_ingredients);

                //addcart
                $("#pro_name").val( product_name );
                $("#pro_price").val( product_cart_price );
                $("#shop_id").val( shop_id );
                $("#pro_id").val( product_id );

                var rel= '';
				var pro_dimension = '';


                var product_cat = $(e.relatedTarget).attr('data-category');
                $.ajax({
                      url: "/related/product",
                      type: "get",
                      data: { 
                        shop_id: shop_id, 
                        category_id: product_cat, 
                      },
                      success: function(response) {
                        console.log(response);
                    $.each( response , function( key, value ) { 

                    $.each( value.products , function( src, dst ) { 


                    rel += '<div class="col-sm-3">\
                                <div class="item">';
                    $.each( dst.featured_images , function( pro, img ) { 

                        rel +=   '<img src='+img.url+' class="img-fluid">';
                    });

                    rel +=   '<div class="p-inner">\
                                            <h5>'+dst.prices.orignal_price+'</h5>\
                                            <h6>'+dst.name+'</h6>\
                                            <div class="cat">'+dst.description+'</div>\
                                        </div>\
                                </div>\
                            </div>';
                   
                    });
                });
                    $('#related_pro').html(rel);
                      },
                      error: function(xhr) {
                        //Do Something to handle error
                      }
                });

				 //product image dimension
				 $.ajax({
                      url: "/product/imageDimension",
                      type: "get",
                      data: { 
                        shop_id: shop_id, 
                        category_id: product_cat, 
                        product_id: product_id
                    },
                      success: function(response) {
                        console.log(response);
                        $.each( response , function( key, value ) { 

                            $.each( value.products , function( src, dst ) { 

                                $.each( dst.images , function( pro, img ) { 

                                 
                                        console.log(img.url);
                                    
                                        pro_dimension += '<img src='+img.url+'>';
                                        // $("#product_image_dimension").attr("src", img.url);


                    
                                });
                            });
                        });
                        $('#product_image_dimension').html(pro_dimension);

                    },
                    error: function(xhr) {
                        //Do Something to handle error
                    }
                });




            });
        });

	
       // product filter start
      $(document).ready(function() {
				// filter_data();
				function filter_data() {
					// $('#show_filter');
						var rel= '';

						var brand = get_filter('brand');
						var nutrition = get_filter('nutrition');
						var sub_cat = get_filter('sub_cat');

						var shop_id = $('#get_shop_id').val();
						var cat_id = $('#get_cat_id').val();

						

						$.ajax({
							url: "/product/filter",
							method: "get",
							data: {
						
							brand: brand,
							nutrition:nutrition,
							shop_id : shop_id,
							category_id : cat_id,
							subcategory: sub_cat
						},
						success: function(response) {

							console.log(response);
                    $.each( response , function( key, value ) { 

                    $.each( value.products , function( src, dst ) { 
						$.each( dst.featured_images , function( pro, img ) { 

					rel+='<div class="col-sm-3">\
				          <div class="item">\
				          	 <div class="overlay">\
								<a class="overlay-btn" href="#"  data-toggle="modal" data-name='+dst.name+' data-img='+img.url+' data-price='+dst.prices.orignal_price+' data-description='+dst.description+' data-shop_id='+dst.shop_id+' data-product_id='+dst.id+' data-pro_cart_price='+dst.prices.orignal_price+' data-directions='+dst.directions+' data-warnings='+dst.warnings+' data-details='+dst.details+' data-ingredients='+dst.ingredients+' data-view_more='+dst.id+' data-related_product='+dst.id+' data-category='+value.id+' data-target="#myModal">View</a>\
								  <form action="{{Auth::guest()?url('mycart'):url('addcart')}}"method="POST">\
													{{csrf_field()}}\
													<input type="hidden"  value='+dst.shop_id+' name="shop_id">\
													<input type="hidden"  value='+dst.id+' name="product_id">\
													<input type="hidden" value="1" name="quantity" class="form-control" placeholder="Enter Quantity" readonly min="1" max="100">\
													<input type="hidden"  value='+dst.name+' name="name">\
													<input type="hidden"  value='+dst.prices.orignal_price+' name="price" />\
													<!-- <button  class="add-btn">@lang('user.add_to_cart')</button> -->\
													<button class="overlay-btn-dark" type="submit">Add to Cart </button>\
												</form>\
					          </div>';

						rel+=	'<img src='+img.url+' class="img-fluid">';
							 });
						rel += ' <div class="p-inner">\
				              <h5>'+dst.prices.orignal_price+'</h5><h6>'+dst.name+'</h6>\
				              <div class="cat">'+dst.description+'</div>\
				            </div>\
				          </div>\
				        </div>';

                    });

                });
                    $('#show_filter').html(rel);
							// $('.filter_data').html(data);

						}
					});
				}
				function get_filter(class_name) {
					var filter = [];
					$('.' + class_name + ':checked').each(function() {
					filter.push($(this).val());
					});
					return filter;
				}
				$('.filter_all').click(function() {
					filter_data();
				});
			
	
		});

// product filter end

</script>
@endsection