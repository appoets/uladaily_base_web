<!DOCTYPE html>
<html lang="en">
<head>
  <title>EZ-Grocery</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="{{ Setting::get('site_favicon', asset('favicon.ico')) }}">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
  <!-- FA icons -->
  <link rel="stylesheet" href="{{ asset('assets/css/all.min.css')}}">
  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/css/owl.transitions.css')}}">

  <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">

  <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css')}}">
  <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/user/css/style.css')}}">
 
</head>
@yield('styles')
<body>
@include('user.notification')

@include('user.layouts.partials.header')

@yield('content')
@include('user.layouts.partials.footer')

  <!-- Jquery -->
  <script src="{{ asset('assets/js/jquery.min.js')}}"></script>

  <!-- Bootstrap -->
  <script src="{{ asset('assets/js/popper.min.js')}}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>

  <!-- Ionicons -->
  <script src="{{ asset('https://unpkg.com/ionicons@5.0.0/dist/ionicons.js')}}"></script>

  <!--slick carousel -->
<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js')}}"></script>
<script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js')}}"></script>



  <!--slick carousel -->
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js')}}"></script>
   <!-- Map JS -->
    <script src="{{  asset('assets/user/js/jquery.googlemap.js')}}"></script>
    
    @include('user.layouts.partials.script')

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
 <!-- Incrementing JS -->
 <script src="{{ asset('assets/user/js/incrementing.js')}}"></script>
  <!-- Scripts -->
  <script src="{{ asset('assets/user/js/scripts.js')}}"></script>
  <script src="{{ asset('assets/user/js/jquery.matchHeight-min.js')}}"></script>

@yield('scripts')

@yield('deliveryscripts')
<script type="text/javascript">
      $(function() {
      $('.equal-height').matchHeight({
      byRow: true,
      property: 'height'
      });
      });
    </script>

<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }

    function triggerHtmlEvent(element, eventName) {
      var event;
      if (document.createEvent) {
        event = document.createEvent('HTMLEvents');
        event.initEvent(eventName, true, true);
        element.dispatchEvent(event);
      } else {
        event = document.createEventObject();
        event.eventType = eventName;
        element.fireEvent('on' + event.eventType, event);
      }
    }

    jQuery('.lang-select').click(function() {
      var theLang = jQuery(this).attr('data-lang');
      jQuery('.goog-te-combo').val(theLang);

      //alert(jQuery(this).attr('href'));
      window.location = jQuery(this).attr('href');
      location.reload();

    });
  </script>
  
 <script type="text/javascript">
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        transitionStyle : "goDown",
        autoPlay:false
    });

    $(".fav-slider").owlCarousel({
        items:4,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        transitionStyle : "goDown",
        autoPlay:false
    });
});
</script>

 <script type="text/javascript">
   $(document).ready(function(){
    $('.customer-logos').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 2
            }
        }]
    });
});
 </script>

 <script type="text/javascript">

  //login page
   $(document).ready(function(){
        $(".signin-step2, .signin-step3").hide();
        $(".btn-login-next1").click(function(){
          event.preventDefault()
          $(".signin-step2").show();
          $(".signin-step1, .signin-step3").hide();
        });
        $(".btn-login-next2").click(function(){
          event.preventDefault()
          $(".signin-step3").show();
          $(".signin-step1, .signin-step2").hide();
        });
    });

   //register page
   $(document).ready(function(){
        $(".signup-step2, .signup-step3, .signup-step4").hide();
        $(".btn-reg-next1").click(function(){
          event.preventDefault()
          $(".signup-step2").show();
          $(".signup-step1, .signup-step3, .signup-step4").hide();
        });
        $(".btn-reg-next2").click(function(){
          event.preventDefault()
          $(".signup-step3").show();
          $(".signup-step1, .signup-step2, .signup-step4").hide();
        });
        $(".btn-reg-next3").click(function(){
          event.preventDefault()
          $(".signup-step4").show();
          $(".signup-step1, .signup-step2, .signup-step3").hide();
        });
    });
 </script>

<!-- image Upload -->
 <script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
      }
          }
      $("#imageUpload").change(function() {
          readURL(this);
      });
 </script>

 <!-- add card page -->
 <script type="text/javascript">
   $(document).ready(function(){
        $(".card-block-2").hide();
        $(".card-next-1").click(function(){
          event.preventDefault()
          $(".card-block-2").show();
          $(".card-block-1").hide();
        });
      });
 </script>
  
<!-- add address page -->
 <!-- <script type="text/javascript">
   $(document).ready(function(){
     $(".address-form").hide();
        $(".add-address").click(function(){
          event.preventDefault()
          $(".address-form").show();
          $(".address-box, .add-address-row").hide();
        });
      });
 </script> -->


  <script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script>

<script type="text/javascript">
  function increaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById('number').value = value;
}

function decreaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 0 : value;
  value < 1 ? value = 1 : '';
  value--;
  document.getElementById('number').value = value;
}

 function increaseValue1() {
  var value = parseInt(document.getElementById('number1').value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById('number1').value = value;
}

function decreaseValue1() {
  var value = parseInt(document.getElementById('number1').value, 10);
  value = isNaN(value) ? 0 : value;
  value < 1 ? value = 1 : '';
  value--;
  document.getElementById('number1').value = value;
}
</script>

<script type="text/javascript">
 
</script>


<!--Store Filter-->

<script type="text/javascript">
   $('.filters ul li').click(function(){
    $('.filters ul li').removeClass('active');
    $(this).addClass('active');
    
    var data = $(this).attr('data-filter');
    $grid.isotope({
      filter: data
    })
  });

  var $grid = $(".grid").isotope({
    itemSelector: ".all",
    percentPosition: true,
    masonry: {
      columnWidth: ".all"
    }
  })
</script>

<!--Search Dropdown-->
<script type="text/javascript">
  $('.dropdown-toggle').click(function(e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).closest('.search-dropdown').toggleClass('open');
});

$('.dropdown-menu > li > a').click(function(e) {
  e.preventDefault();
  var clicked = $(this);
  clicked.closest('.dropdown-menu').find('.menu-active').removeClass('menu-active');
  clicked.parent('li').addClass('menu-active');
  clicked.closest('.search-dropdown').find('.toggle-active').html(clicked.html());
});

$(document).click(function() {
  $('.search-dropdown.open').removeClass('open');
});
</script>



<!-- Product image Script -->

<script type="text/javascript">
  $(".mini img").click(function(){  

 $(".maxi").attr("src",$(this).attr("src").replace("100x100","400x400"));

});
  $('.icon-wishlist').on('click', function(){
  $(this).toggleClass('in-wishlist');
});
</script>

<script type="text/javascript">
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    pagination:false,
    navigation:true,    
    navigationText:["",""],
    transitionStyle : "goDown",
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
</script>

</body>
</html>
